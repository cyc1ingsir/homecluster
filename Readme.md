# Setting up a K3s-Cluster on 1+ Raspberry Pi (4B)

**With K3s, MetalLB, Longhorn, Grafana, Prometheus, Pi-Hole**

<img src="./images/monitoring-dashboard.png" alt="One of the predefined Grafana dashboards." style="height: 300px; "/>

## Table of content

[[_TOC_]]

## About

This is an opinionated (but not complete) setup guide for running a Kubernetes (K8s) cluster using the lightweight variant K3s at home. It started with a single Raspberry Pi 4B (8GB) but a second RasPi 4B (4GB) was added as a worker node later on.

It's mainly been setup for learning about Kubernetes but I might extend this to host a few tiny apps running on two SBCs atm.

These are not a complete step by step instruction but links have been added to other projects and blog posts used when setting it up. If you want to follow this guide, I recommend following the references to read more about the background and for the extended documentations of the tools used.

Whilst the Helm charts used don't need to be cloned, a few of the projects need to be cloned.
These may be cloned from inside this project as they are included within the [.gitignore](.gitignore).

# Prerequisites

## Tools needed on local machine
- ansible (`zypper in ansible`)
- helm (`zypper in helm`)
- Pi Imager (`zypper in rpi-imager`)

## Setup
`~/.ssh/config`
```
Host k8scluster-server
    Hostname 192.168.xxx.xxx
    User pi
    IdentityFile ~/.ssh/id_xxx
```

## Raspi OS - Installation

Assumption: Raspberry 4B with > 2GB RAM (No other config has been tested but will probably work)

### Image

Install RaspiOS on your SD-Card or SSD [SSD-USB-Boot](https://jamesachambers.com/new-raspberry-pi-4-bootloader-usb-network-boot-guide/) and [Benchmark](https://pibenchmarks.com/)... e.g. with Raspberry Pi Imager (https://www.raspberrypi.com/news/raspberry-pi-imager-imaging-utility/) (`zypper in rpi-imager`) and an image of your choice e.G.:
 [RaspiOS Download (64bit lite)](https://downloads.raspberrypi.org/raspios_lite_arm64/images/)

Inside Imager use [Ctrl]+[Shift]+[X] to select options: 
- Set hostname (different names for Server and Agent-Nodes)
- Enable SSH
  - Allow public-key authentication (set key!)
- Set locale settings

### Hostnames

e.g 
- k8scluster-server
- k8scluster-agent-01

### boot/config.txt

```
dtparam=act_led_trigger=heartbeat
dtparam=pwr_led_trigger=panic
```
_Optional_ (for booting from SSD):
```
rpi-eeprom-config -e
BOOT_ORDER=0xf41
```
**ToDo**: Boot agent node from network (e.g. from Master)

### Boot!

Boot fresh image and verify. If your ssh-key doesn't work you might have to copy another one `ssh-copy-id pi@<pi-ip-addr>`.

### Fixed-IP
`vi /etc/dhcpcd.conf`
```
interface eth0
static ip_address=192.xxx.xxx.xxx/24
static routers=192.xxx.xxx.1
static domain_name_servers=192.xxx.xxx.xxx #?? -> PiHole later?
```

# Home Cluster with K3s

## K3s

Install [K3s](https://k3s.io/) on the Raspberry Pis.
(Inspired by: https://www.jeffgeerling.com/blog/2020/installing-k3s-kubernetes-on-turing-pi-raspberry-pi-cluster-episode-3)

### K3s ansible

Preferred method is using Ansible with this repo:
[Ansible K3s](https://github.com/k3s-io/k3s-ansible)    
```
git clone git@github.com:k3s-io/k3s-ansible.git
```

#### Preparation

Create an inventory reflecting your local settings (sample provided inside `k3s-ansible-config`):

```bash
cd k3s-ansible/inventory
cp -R sample homecluster
```
Inside this new directory `hosts.ini` and `group_vars/all.yml` needs to be modified. (see below for finding the IPs of the Pis)
```bash
nmap -sn 192.168.xxx.xxx/24
```

In `all.yaml` check the settings of `k3s_version` matches a recent [K3s-Release](https://github.com/k3s-io/k3s/releases) and that `ansible_user` matches your pi user!   
_Optional_: `extra_server_args: "--disable servicelb --disable traefik"` (Traefik is the Ingress controller (you might want to install a different one) and servicelb will be replaced be MetalLb below.)

_Optional_: In Order to install [Longhorn](https://longhorn.io) on the nodes, an additional package is needed. 
Add the code block below to `roles/raspberrypi/tasks/prereq/Longhorn.yml`

```yaml
---
# https://longhorn.io/docs/1.2.2/deploy/install/#installation-requirements
- name: Install open-iscsi needed for Longhorn
  apt:
    name: open-iscsi
```
and add this at the end of `roles/raspberrypi/tasks/main`
```yaml
- name: install Longhorn
  include_tasks: "prereq/Longhorn.yml"
  when:
    - raspberry_pi|default(false)
```


Check for this PR: https://github.com/k3s-io/k3s-ansible/pull/151 (should make it Bullsey compatible)

#### Installation

```bash
ansible-playbook site.yml -i inventory/homecluster/hosts.ini
scp pi@k8scluster-server:~/.kube/config ~/.kube/config-homecluster # kubectl with context ...
# export KUBECONFIG=~/.kube/config-homecluster
kubectl get nodes

# Resetting:
# ansible-playbook reset.yml -i inventory/homecluster/hosts.ini

# Securely shutdown
# ansible all -i inventory/homecluster/hosts.ini -a "shutdown now" -b
```

## MetalLB - Loadbalancer

[MetalLB - bare metal load balancer](https://metallb.universe.tf/installation/)

Inspired from e.g. (https://github.com/xposix/k3s_pihole) to use this for Pi-Hole (for exposing ports instead of http(s) by an ingress).

```bash
helm repo add metallb https://metallb.github.io/metallb
helm repo search metallb
helm repo update
helm install -n metallb-system  metallb metallb/metallb -f ./metallb_values.yaml --create-namespace
```

metallb_values:
```yaml
configInline:
  address-pools:
   - name: default
     protocol: layer2
     addresses:
     - 192.168.xxx.xxx-192.168.xxx.xxx
```
### Operator

[MetalLB-K8s-Operator but WIP](https://github.com/metallb/metallb-operator) is worth watching though.

### Literature
https://opensource.com/article/20/7/homelab-metallb
https://serverfault.com/questions/1070622/why-is-metallb-automatically-routing-node-ip-to-an-specific-service-ip
https://rancher.com/docs/k3s/latest/en/networking/

## Longhorn - Block Volumes

[Longhorn - Cloud native distributed block storage for Kubernetes](https://longhorn.io)

https://longhorn.io/docs/1.2.2/deploy/install/install-with-helm/

```
helm repo add longhorn https://charts.longhorn.io
helm repo update
helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace
kubectl -n longhorn-system get pod
```

```
NAME: longhorn
LAST DEPLOYED: Wed Nov 10 22:05:09 2021
NAMESPACE: longhorn-system
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Longhorn is now installed on the cluster!

Please wait a few minutes for other Longhorn components such as CSI deployments, Engine Images, and Instance Managers to be initialized.

Visit our documentation at https://longhorn.io/docs/
```

Watch out! You may now have two default storage classes:
```
> kubectl get sc
NAME                   PROVISIONER             RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGElocal-path (default)   rancher.io/local-path   Delete          WaitForFirstConsumer   false                  44h
longhorn (default)     driver.longhorn.io      Delete          Immediate              true                   19h
```
So you may edit one of it (eg `kubectl edit sc local-path`) and set the `storageclass.kubernetes.io/is-default-class` from `"true"` to `""`.

Creating a StorageClass to be used from Grafana and Prometheus and one from Pi-Hole (see below) with   
```
kubectl apply -f longhorn-storage-class.yaml
kubectl apply -f longhorn-storage-class-pi-hole.yaml
```
from inside the longhorn folder.

```
k get pvc --all-namespaces -o wide
NAMESPACE    NAME                                 STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS    AGE     VOLUMEMODE
pihole       pihole                               Bound    pvc-76610852-bfb1-499f-aa2a-f2b3cc43ccb3   500Mi      RWO            pihole-sc       10m     Filesystem
monitoring   grafana-storage                      Bound    pvc-56db64ab-9ff0-4786-bfa1-26de83c55e10   8Gi        RWO            monitoring-sc   2m16s   Filesystem
monitoring   prometheus-k8s-db-prometheus-k8s-0   Bound    pvc-a37a572c-cf92-4771-8328-47eb0c73c002   2Gi        RWO            monitoring-sc   2m11s   Filesystem
```

### UI

You may install an ingress for accessing the Longhorn UI:
```
kubectl apply -f longhorn-ui-ingress.yam
```
### Volume Path

Volumes will be created at the path ` /var/lib/longhorn/replicas` and may be mounted to inspect via `  mount pvc-76610852-xxxx/volume-head-000.img /mnt/`.
### ToDo

- How will volumes survive cluster reinstallation (Backup and Restore)?
- How do you exclude certain nodes (e.g. the ones running from SD-Card) from being used for providing storage space at installation time.

## Monitoring (Grafana/Prometheus/Alertmanager)

A wonderful cluster monitoring project was created by Carlos Eduardo which is simply used here.
https://github.com/carlosedp/cluster-monitoring

#### Prerequisite
Go needs to be installed.
```
git clone https://github.com/carlosedp/cluster-monitoring
cd cluster-monitoring
make vendor
# configure vars.jsonnet files... see next chapter
make
```

#### Configuration

Adopt `vars.jsonnet` to local environment. Follow the exhaustive Readme.
Changes made:
- smtpRelay.enbled: true ?? -> run scripts/create_gmail_auth.sh
- k3s.enabled: true
- armExporter.enabled: true
- metallbExporter.enabled: true
- traefikExporter.enabled: true
- k3s.master_ip: ['192.168.xxx.xxx']
- suffixDomain: 'kube',
- prometheus.retention: 15d

#### Persistent Volumes

In case you want provide persistent storage for Prometheus and Grafana and don't want the volumes to be created dynamically via a storage class (see above), you need to provision two volumes sort of manually by
provide and apply two PersistentVolume resources based on the yaml config files inside the sample subfolder.for examples.
- enablePersistence
  - prometheus: true,
  - grafana: true,
  - in case of pre-created PV only, replace '' with pv name (and storageClass below ''!)
    - prometheusPV: '',
    - grafanaPV: '',
  -  to use a specific storageClass only (and PV names ''!)
    - storageClass: 'monitoring-sc',
  - prometheusSizePV: '2Gi',
  -grafanaSizePV: '8Gi',

#### Installation
```
kubectl apply -f manifests/setup/
kubectl apply -f manifests/
```

#### Uninstall 

`make teardown`

#### Change Ingress URLs

In case ingress URLs where changed only:
`make change_suffix suffix=[suffixURL]`

## Pi-Hole

[Pi-Hole](pi-hole.net) 
(With inspiration from https://www.jeffgeerling.com/blog/2020/raspberry-pi-cluster-episode-4-minecraft-pi-hole-grafana-and-more)

_Optional for inspiration:_
```
git clone git@github.com:xposix/k3s_pihole.git
```

#### Configuration

Create your own pihole-values.yaml. You can start from here:
https://artifacthub.io/packages/helm/mojo2600/pihole
You'll find more adjustable values with the default spec here:
https://github.com/MoJo2600/pihole-kubernetes/blob/master/charts/pihole/values.yaml

```yaml
image:
  tag: "2021.10.1"
  pullPolicy: IfNotPresent

dnsmasq:
  customDnsEntries:
    - address=/kube/192.168.1.122

persistentVolumeClaim:
  enabled: true
  storageClass: "pihole-sc"
```
Stored as pihole-values.yaml

#### Installation

```bash
helm repo add mojo2600 https://mojo2600.github.io/pihole-kubernetes/
help repo update
helm install -n pihole  pihole mojo2600/pihole -f pihole-values.yaml --create-namespace
```
Inside FritzBox:
- Internet > Zugangsdaten > DNS-Server
- Heimnetz > Network > Network Settings > IPv4-Settings > local DNS-Server
  
### Literature
https://docs.pi-hole.net/ftldns/dns-cache/

## Using self created docker images / Local Docker Registry

### Local docker registry 
**WIP** (see literature!)

With custom values for configuring !!
```console
helm repo add docker-registry https://bh90210.github.io/docker-registry-chart
helm update
helm install docker-registry docker-registry/docker-registry --create-namespace -n docker-registry -f Registry/values.yaml
```
#### Persistent Volumes

Create StorageClass used in inside customized values.yaml:
```
kubectl apply -f Longhorn/longhorn-storage-class-registry.yaml
```

#### Securing / TLS
(The `containerd` seems to use the CAs known to the host machines. Even though, it's possible to create your own self signed certificate and maybe even create your own CA, some certificate and key files needed to be transferred to the host machines and probably to all nodes as well.)

```
# inside Registry folder:
openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout registry.key \
  -addext "subjectAltName = DNS:registry.kube" \
  -x509 -days 365 -out registry.crt

cd CA
ansible-playbook site.yaml

kubectl create secret tls registry-tls --key="registry.key" --cert="registry.crt" -n docker-registry
kubectl get secret registry-tls -n docker-registry -o yaml
```
Own (intermediate) CA
https://www.golinuxcloud.com/openssl-create-certificate-chain-linux/

**K3s - Private Registry Configuration**  
on main server and nodes:
>`registries.yaml` file exists at `/etc/rancher/k3s/`
https://rancher.com/docs/k3s/latest/en/installation/private-registry/

**ToDo**
- customize values
  - ~PV~
  - Ingress, password?
- How to upload/list images? (see Readme inside Testing/echoserver)
- TLS, DNS, using images for deployments?
- using uploaded base images for building own app images
- How will content inside the registry survive a helm/upgrade

### Alternative Method - Upload images into cluster directly
Transfer baked images into cluster (k3s ctr)

https://cwienczek.com/2020/06/import-images-to-k3s-without-docker-registry/
Example:
```
sudo k3s ctr images import /home/pi/test-app-v1.0.0.tar
```
### Literature

- https://github.com/bh90210/docker-registry-chart (Extensive Documentation)
- https://github.com/twuni/docker-registry.helm (With interesting PRs eg https://github.com/twuni/docker-registry.helm/pull/40)
- https://cloud.google.com/artifact-registry/docs/helm (Storing Helm charts within registry)
- https://github.com/distribution/distribution
  