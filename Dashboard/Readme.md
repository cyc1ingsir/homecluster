https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/

```fish
# fish shell version:
set VERSION_KUBE_DASHBOARD (curl -w '%{url_effective}' -I -L -s -S $GITHUB_URL/latest -o /dev/null | sed -e 's|.*/||')
wget https://raw.githubusercontent.com/kubernetes/dashboard/$VERSION_KUBE_DASHBOARD/aio/deploy/recommended.yaml`
```

`kubectl apply -f admin-user.yaml`   

`kubectl -n kubernetes-dashboard describe secret admin-user-token | grep '^token'`   


```bash
kubectl delete ns kubernetes-dashboard
kubectl delete clusterrolebinding kubernetes-dashboard
kubectl delete clusterrole kubernetes-dashboard
```


