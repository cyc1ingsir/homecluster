https://galaxy.ansible.com/community/crypto?extIdCarryOver=true&sc_cid=701f2000001OH7YAAW
```console
ansible-galaxy collection install community.crypto
```
https://docs.ansible.com/ansible/latest/collections/community/crypto/docsite/guide_ownca.html

### Which file contains what crt, csr, key, pem
https://crypto.stackexchange.com/questions/43697/what-are-the-differences-between-pem-csr-key-crt-and-other-such-file-exte


On master _and?_ node inside
`/etc/rancher/k3s/registries.yaml`

```yaml
mirrors:
  registry.kube:
    endpoint:
      - "https://registry.kube:5000"
configs:
  "registry.kube:5000":
    tls:
#      insecure_skip_verify: true
      cert_file: /etc/rancher/k3s/certs/registry.pem
      key_file:  /etc/rancher/k3s/certs/registry.key
      ca_file:   /etc/rancher/k3s/certs/kube-ca-certificate.pem
```

These files need to be copied to the nodes.
> **Todo:**  Extend the ansible-playbook to copy the above files to the folder including the registries.yaml as a template