```
openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout registry.key \
  -addext "subjectAltName = DNS:registry.kube" \
  -x509 -days 365 -out registry.crt

kubectl create secret tls registry-tls --key="registry.key" --cert="registry.crt" -n docker-registry
kubectl get secret registry-tls -n docker-registry -o yaml
```
this secret will be used for accessing the registry via https  

```
http -v https://registry.kube:5000/v2/ --verify=registry.crt
```
This doesn't help when pushing and pulling images to and from the registry though.