Experimenting with the cluster internal registry:

As long, trusting the certificates from the registry inside the cluster from the local dev machine hasn't been resolved, this line needs to be added to the file `/etc/docker/daemon.json`
```json
{
    "insecure-registries" : ["registry.kube:5000"]
}
```

```console
# pulling a simple arm64 image
docker pull e2eteam/echoserver:2.2-linux-arm64

# tagging it for pushing it to cluster registry
docker tag e2eteam/echoserver:2.2-linux-arm64 registry.kube:5000/e2eteam/echoserver:2.2-linux-arm64

# pushing it
docker push registry.kube:5000/e2eteam/echoserver:2.2-linux-arm64
# 6b0f279fa1c2: Pushed
# ...
# 2.2-linux-arm64: digest: sha256:182... size: 2820

# from inside the Registry subfolder where the self signed certificate lives
http -v https://registry.kube:5000/v2/_catalog --verify=registry.crt
```
Will hopefully provide a result similar to this:
```json
{
    "repositories": [
        "e2eteam/echoserver",
    ]
}
```

https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/
