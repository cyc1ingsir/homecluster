Refer to instructions from [MetalLB-Section in main document](../Readme.md#MetalLB-Loadbalancer)

Adopt the configuration inside the [metallb values](./metallb_values.yaml) to meet your environment.

```bash
helm install -n metallb-system  metallb metallb/metallb -f ./metallb_values.yaml --create-namespace
```

```
NAME: metallb
LAST DEPLOYED: Wed Nov 10 16:27:45 2021
NAMESPACE: metallb-system
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
MetalLB is now running in the cluster.
LoadBalancer Services in your cluster are now available on the IPs you
defined in MetalLB's configuration:

config:
  address-pools:
  - addresses:
    - 192.168.1.120-192.168.1.130
    name: default
    protocol: layer2

To see IP assignments, try `kubectl get services`.
```
